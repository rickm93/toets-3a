Toets blok 3a Versie 1.0 17-4-2016

Dit script is volledig geschreven in python 3.4

Voor de nieuwste versie van deze readme kijkt u op de
bitbuckit pagina: https://bitbucket.org/rickm93/toets-3a/src

-------------------------------------------------------------
I. Betanden:
-------------------------------------------------------------

- OWE3a-PraktischeOpdracht1516.py
- OWE3a-PraktischeOpdracht1516.pdf
- ploop.fa
- Readme.txt
- gpl EN.pdf
- gpl NL.pdf

-------------------------------------------------------------
II. Functie/doel:
-------------------------------------------------------------

Script kijkt in een opgegeven fasta bestand welk P-loop 
consensus patroon het meest voorkomt in de mens en zoekt 
aan de hand van dit patroon andere soorten die hier aan 
voldoen en voegt dan de soort en het accessienummer toe
aan een dictionary en print deze gegevens.

-------------------------------------------------------------
III. Voor start:
-------------------------------------------------------------

Voordat u dit script kunt uitvoeren dient u een programma 
te hebben waarmee u het script kunt uitvoeren. Een minimale 
vereiste is de IDLE van python 3.4. Dit kunt u downlaoden op:
<https://www.python.org/downloads/>.

Voordat het script wordt gestart dient het script + het 
fasta bestand ploop.fa in dezelfde map aanwezig te zijn. Is 
dit niet het geval dan dient in de code de lokatie van het 
ploop.fa bestand aangepast te worden. 

Open het script OWE3a-PraktischeOpdracht1516.py met de IDLE
en zoek de regel:

data = getData("ploop.fa")

Verander ploop.fa naar de lokatie waar u het ploop.fa
bestand heeft opgeslagen. Als u bijvoorbeeld het ploop.fa
bestand in de map Downloads hebt opgeslagen dan ziet het er 
als volgt uit:

data = getData("C:\\Users\\Username\\Downloads\\ploop.fa")

Let op: Zorg wel dat u het pad naar het bestand weet.

-------------------------------------------------------------
IV. Start:
-------------------------------------------------------------

U heeft de python 3.4 IDLE (of een andere IDLE) het script en het 
ploop.fa bestand op u computer staan. Om nu het script uit te 
voeren klikt u in de IDLE op Run en daarna op Run Module 
(u kunt ook op F5 drukken). Het script wordt nu uitgevoerd en u 
krijgt iets te zien zoals onderstaand:

Species                Accesion number
--------------------------------------
Mus                    Q9DC29
Mycobacterium          O65934
Dictyostelium          Q9NGP5
Oryctolagus            Q9N0V3
Arabidopsis            Q9SW08
Homo                   Q9NP58
Rattus                 O70595

-------------------------------------------------------------
V. Vraag & Antwoord:
-------------------------------------------------------------

V: Ik krijg de volgende melding "Het bestand kon niet gevonden 
worden!" hoe komt dit?
A: De locatie van het ploop.fa bestand kan niet gevonden worden. 
Kijk nog een keer goed naar punt III in deze readme.

V: Waarom krijg ik bijvoorbeeld maar 1 keer mus te zien in de 
lijst terwijl er op ze minst 3 mussen voor komen in het ploop.fa 
bestand?
A: Tijdens het maken van dit script is er voor gekozen om van 
elke soort die voor komt in het bestand ploop.fa de eerste van 
een soort die voldoet aan het patroon op te nemen in de lijst.

V: Waarom wordt er maar 1 patroon van de mens geselecteerd en 
niet allemaal?
A: Tijdens het maken van dit script is er voor gekozen om het 
meest voorkomende patroon in de mens te selecteren voor de 
verdere zoektocht in de overige sequenties. Deze keuze is gemaakt 
omdat het meest voorkomende patroon in de mens waarschijnlijk 
ook in de meeste andere sequenties voor komt.

-------------------------------------------------------------
VI. Contact:
-------------------------------------------------------------

Naam: 	Rick Medemblik
E-mail: R.Medemblik@student.han.nl
		snorkelen_123fan@live.nl

-------------------------------------------------------------
VII. Copyright
-------------------------------------------------------------

Copyright (C) 2016 Rick Medemblik, Lelystad

Nederlands:

Dit programma is vrije software: je mag het herdistribueren en/of 
wijzigen onder de voorwaarden van de GNU Algemene Publieke 
Licentie zoals gepubliceerd door de Free Software Foundation, 
onder versie 3 van de Licentie of (naar jouw keuze) elke latere versie.

Dit programma is gedistribueerd in de hoop dat het nuttig zal zijn 
maar ZONDER ENIGE GARANTIE; zelfs zonder de impliciete garanties 
die GEBRUIKELIJK ZIJN IN DE HANDEL of voor BRUIKBAARHEID VOOR EEN 
SPECIFIEK DOEL. Zie de GNU Algemene Publieke Licentie voor meer details.

Je hoort een kopie van de GNU Algemene Publieke Licentie te hebben
ontvangen samen met dit programma. Als dat niet het geval is, zie
<http://www.gnu.org/licenses/>. 

Engels:

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

