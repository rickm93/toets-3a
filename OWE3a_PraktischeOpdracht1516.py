"""
Titel:      Toets OWE3a schooljaar 2015-2016
Doel:       Script kijkt in een opgegeven fasta bestand
            welk P-loop consensus patroon het meest
            voorkomt in de mens en zoekt aan de hand van
            dit patroon andere soorten die hier aan voldoen
            en voegt dan de soort en het accessienummer toe
            aan een dictionary en print deze gegevens.
Bronnen:    Eerder gemaakte scripts en het internet
Auteur:     Rick Medemblik
<<<<<<< HEAD
Datum:      17-04-2016
Versie:     3.0
"""

import re
from collections import Counter

def main():
    data = getData("ploop.fa")       # Hier komt het te analyserende bestand.
    species = getpatroon(data)
    lijst(species)

class organisme:
    def __init__(self, accessienummer, soort, sequentie):
        self.__accessienummer = accessienummer
        self.__soort = soort
        self.__sequentie = sequentie
    def accessienummer(self):
        return self.__accessienummer
    def Soort(self):
        return self.__soort
    def Sequentie(self):
        return self.__sequentie

"""
Deze functie extraheert de sequentie, soort en accessienummer uit het bestand ploop.fa
en slaat dit op in de lijst data en retuneerd dit dan weer.
Er is een FileNotFoundError toegepast om de error op te vangen als het bestand niet
aanwezig is. Ook is er een except toegepast om alle overige error's op te vangen.
"""

def getData(bestand):
    try:
        with open(bestand, "r") as bestand:         # Hier wordt het bestand geopend.
            data = []                               # Onder data wordt de accesienummer en soort opgeslagen.
            sequentie = ""                          # Onder sequentie wordt de sequentie opgeslagen.
            startlezen = False                      # Startlezen staat nog op False omdat we nog niet begonnen zijn.
            regelnummer = 0                         # Regelnummer staat nog op 0 omdat de eerste regel nog moet komen.

            for regel in bestand:
                if regel[0] == ">":                 # Staat het > in de regel dan zetten we startlezen op True omdat dit het begin is.
                    startlezen = True
                if startlezen:
                    regelnummer += 1
                    if regel[0] == ">":             # Staat het > in de regel en is het de eerste regel dan halen we uit deze regel het accessienummer.
                        accessienummer = regel[4:regel.find("|",4)]
                    if regelnummer == 2:            # Zijn we bij regel nummer 2 dan halen we de soort uit deze regel.
                        soort = regel[regel.find("[")+1:regel.find("]")]
                        soort = soort.split(" ")[0]
                    if regelnummer >= 3 and not("-" in regel and ":" in regel): # Staat er geen - of : in de regel dan is het de sequentie en voegen we dit toe aan de variabele sequentie.
                        sequentie += regel
                    if "-" in regel and ":" in regel:           # Staat er wel een - of : in de regel dan is de sequentie afgelopen en beginnen we weer opnieuw.
                        sequentie = sequentie.replace("\n","")
                        data.append(organisme(accessienummer, soort, sequentie))
                        sequentie = ""
                        startlezen = False
                        regelnummer = 0

            bestand.close()

            return data             # Hier returnen we de data.

    except FileNotFoundError:
        print('Het bestand kon niet gevonden worden!')

    except:
        print("Er is een onbekende fout opgetreden.")

"""
Deze functie kijkt voor elke sequentie in de lijst data of het patroon
[AG]−x(4)−G−K−[ST] voor komt in de sequentie. Komt het patroon voor
in de sequentie dan wordt van deze sequentie de soort en het accessienummer
aan de dictionary species toegevoegd (op voorwaarde dat de soort niet al
een keer voor komt). Hierna wordt 1 bij de index opgeteld en begint alles
weer opnieuw tot dat alle sequenties op zijn. Na dat alle sequentie op zijn
wordt de dictionary species geretuneerd.
"""

class ploop_patroon:
    def __init__(self, species):
        self.__species = species
    def patroon(self):
        return self.__species

def getpatroon(data):
    index = 0
    patroon = ""
    species = {}

    for soort in data:
        soort = data[index].Soort()                                         # Uit de variabele data uit de functie getData halen we de soort en voegen we dit toe aan de variabele soort.

        if "Homo" in soort:
            sequentie = data[index].Sequentie()                             # Uit de variabele data uit de functie getData halen we de sequentie en voegen we dit toe aan de variabele sequentie.

            searchObj = re.search(r"[AG].{4}GK[ST]", sequentie)             # Er wordt gekeken of het patroon [AG]−x(4)−G−K−[ST] voorkomt in de sequentie.

            if searchObj:
                patroon += searchObj.group()            # Komt het patroon voor dan voegen we hem toe aan de lijst patroon.

        index += 1              # Is al het bovenstaande klaar dan tellen we 1 bij de index op en beginnen we weer opnieuw.

    if patroon is not "":           # Is de bovenstaande for loop klaar en de variabele patroon niet meer leeg dan zetten we de index weer op 0.
        index = 0

    lijst_patroon = [patroon[i:i + 8] for i in range(0, len(patroon), 8)]           # Alle gevonden patronen zitten nu nog aan elkaar in de functie patroon deze string hakken we in stukken van 8.
    mcommon = [ite for ite, it in Counter(lijst_patroon).most_common(1)]            # Hier wordt gekeken wat de meest voorkomende patroon in de variable lijst_patroon is.

    for sequentie in data:
        sequentie = data[index].Sequentie()             # Uit de variabele data uit de functie getData halen we de sequentie en voegen we dit toe aan de variabele sequentie.

        searchObj = re.search(mcommon[0], sequentie)         # Er wordt gekeken of het patroon uit de variable mcommon voorkomt in de sequentie.

        if searchObj:                                               # Komt het patroon voor in de sequentie en staat het nog niet in de lijst species dan voegen we hem toe.
            if data[index].Soort() not in species:
                species[data[index].Soort()] = data[index].accessienummer()

        index += 1          # Is al het bovenstaande klaar dan tellen we 1 bij de index op en beginnen we weer opnieuw.

    return species          # Hier returenen we de dictionary species.

"""
Deze functie print de data uit de dictionary species.
"""

class lijst:
    def __init__(self, lijst):
        self.__lijst = lijst

def lijst(species):
    print("Species" + " " * 16 + "Accesion number")         # Er worden hier tussen Species en Accesion number 16 spaties geplaatst.
    print("-" * 38)                                         # Er worden na de eerste regel 38 - geplaatst.
    for soort, code in species.items():
        seperate = 13 - len(soort) + 10                     # Er wordt per soort gekeken hoeveel spaties er geplaats moeten worden.
        print(str(soort) + " " * seperate + str(code))      # Hier worden de soort en de accessienummer geprint met het aantal berekende spaties er tussen.

main()
